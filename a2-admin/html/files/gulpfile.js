var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var prefix = require('gulp-autoprefixer');
var minify = require('gulp-clean-css');
var rename = require('gulp-rename');


var styleConfig = {
	sassMain: 'dev/sass/style.scss',
	sassFiles: 'dev/sass/**/*.scss',
	cssfiles: ['assets/css/style.css'],
	dest: 'assets/css',
	autoprefixerOptions: {
		browsers: ['last 2 versions', '> 5%']
	}
};

var bootstrapConfig = {
	sassMain: 'dev/sass/bootstrap.scss',
	sassFiles: 'dev/sass/**/*.scss',
	cssfiles: ['assets/css/bootstrap.css'],
	dest: 'assets/css',
	autoprefixerOptions: {
		browsers: ['last 2 versions', '> 5%']
	}
};

function style() {
	return(
		gulp
		.src(styleConfig.sassFiles)
		.pipe(sass().on('error', sass.logError))
		.pipe(prefix(styleConfig.autoprefixerOptions.browsers))
		.pipe(minify())
		.pipe(rename({
			extname: '.min.css'
		}))
		.pipe(gulp.dest(styleConfig.dest))
	);
}

function bootstrap() {
	return (
		gulp
		.src(bootstrapConfig.sassMain)
		.pipe(sass().on('error', sass.logError))
		.pipe(prefix(bootstrapConfig.autoprefixerOptions.browsers))
		.pipe(gulp.dest(bootstrapConfig.dest))
		.pipe(rename({
			extname: '.min.css'
		}))
		.pipe(minify())
		.pipe(gulp.dest(bootstrapConfig.dest))
	);
}

function watch() {
	gulp.watch(styleConfig.sassFiles, style);
	gulp.watch(bootstrapConfig.sassFiles, bootstrap);
}

exports.watch = watch;