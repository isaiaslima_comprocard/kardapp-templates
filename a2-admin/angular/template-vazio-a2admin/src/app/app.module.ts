import { BrowserModule }	from '@angular/platform-browser';
import { RouterModule }	from '@angular/router';
import { NgModule }	from '@angular/core';
import { MaterialModule } from './pages/material-components/material.module';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { BrowserAnimationsModule }	from '@angular/platform-browser/animations';


import { ChartsModule }	from 'ng2-charts';
import { CalendarModule }	from 'angular-calendar';
import { AgmCoreModule }	from '@agm/core';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

import { AppRoutingModule }	from './app-routing.module';

import { AppComponent }	from './app.component';
import { DefaultLayoutComponent }	from './layouts/default/default.component';
import { ExtraLayoutComponent }	from './layouts/extra/extra.component';

// A2 Components
import { NavbarComponent }	from './a2-components/navbar/navbar.component';
import { SidebarComponent }	from './a2-components/sidebar/sidebar.component';
import { LogoComponent }	from './a2-components/logo/logo.component';
import { MainMenuComponent }	from './a2-components/main-menu/main-menu.component';
import { A2CardComponent }	from './a2-components/card/card.component';
import { AlertComponent }	from './a2-components/alert/alert.component';
import { BadgeComponent }	from './a2-components/badge/badge.component';
import { BreadcrumbComponent }	from './a2-components/breadcrumb/breadcrumb.component';
import { FileComponent }	from './a2-components/file/file.component';
import { NIHTimelineComponent }	from './a2-components/ni-h-timeline/ni-h-timeline.component';
import { FooterComponent }                  from './a2-components/footer/footer.component';
import { AdditionNavbarComponent }          from './a2-components/addition-navbar/addition-navbar.component';
import { HomeComponent } from './pages/home/home.component';

@NgModule({
  imports: [
    BrowserModule,
		FormsModule,
		HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ChartsModule,
    CalendarModule.forRoot(),
		MaterialModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAU9f7luK3J31nurL-Io3taRKF7w9BItQE'
    }),
    LeafletModule.forRoot()
  ],
  declarations : [
    AppComponent,
    DefaultLayoutComponent,
		ExtraLayoutComponent,
		
    NavbarComponent,
    SidebarComponent,
    LogoComponent,
    MainMenuComponent,
    A2CardComponent,
    AlertComponent,
    BadgeComponent,
    BreadcrumbComponent,
    FileComponent,
		NIHTimelineComponent,
    FooterComponent,
    AdditionNavbarComponent,
    HomeComponent
	],
	exports: [
		MaterialModule
	],
  entryComponents: [],
  bootstrap: [ AppComponent ]
})

export class AppModule {

}
